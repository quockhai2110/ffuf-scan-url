import os
import configparser
import json


#Đọc file config
config_obj = configparser.ConfigParser()
config_obj.read("config.ini")

info = config_obj["ffuf"]

os.chdir('/home/quockhai/Desktop/Project/recon-tool')

#Danh sách payload
listdir = '/home/quockhai/Desktop/Project/recon-tool/SecLists/Discovery/Web-Content/directory-list-lowercase-2.3-small.txt'


#Đọc file url (Các URL cần scan)
# list_url = open(info["list_url"], "r")

# list_url_readlines = list_url.readlines()

# for i in list_url_readlines:
#     i = i.replace("\n","FUZZ")
#     cmd = "ffuf -u " + i + " -w " + listdir + " -ic" + " -o " + info["file_output"] + " -mc 200 -recursion"
#     os.system(cmd)
    
    
#Đọc file report và in kết quả ra màn hình
report = open(info["file_output"], "r")

report_readlines = report.readlines()

report_readlines_json = json.loads(report_readlines[0])

print("Các thư mục tìm được: \n")


for i in report_readlines_json:
    print(i)


