import os
import configparser
import json
from recon_directory import recon_directory


if __name__ == '__main__':
    #Đọc file config
    config_obj = configparser.ConfigParser()
    config_obj.read("config.ini")

    config_info = config_obj["ffuf"]

    #Danh sách payload
    list_payload = '/home/quockhai/Desktop/Project/recon-tool/SecLists/Discovery/Web-Content/directory-list-lowercase-2.3-small.txt'

    quockhai = recon_directory( config_info, list_payload)
    #Run ffuf
    quockhai.readfile_in_url_and_using_ffuf()

    #In kết quả ra màn hình
    quockhai.readfile_report_and_print()

    