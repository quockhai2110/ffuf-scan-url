import configparser
import os
import json

class recon_directory:
    
    
    def __init__(self, config_info, list_payload):
        self.config_info = config_info
        self.list_payload = list_payload
        pass

    def readfile_in_url_and_using_ffuf(self):
        #Đọc file url (Các URL cần scan)
        list_url = open(self.config_info["list_url"], "r")

        list_url_readlines = list_url.readlines()

        for i in list_url_readlines:
            i = i.replace("\n","FUZZ")
            cmd = "ffuf -u " + i + " -w " + self.list_payload + " -ic" + " -o " + self.config_info["file_output"] + " -mc 200 -recursion"
            os.system(cmd)


    def readfile_report_and_print(self):
        #Đọc file report và in kết quả ra màn hình
        #Đọc file report và in kết quả ra màn hình
        report = open(self.config_info["file_output"], "r")

        report_readlines = report.readlines()

        report_readlines_json = json.loads(report_readlines[0])

        print("Các thư mục tìm được: \n")

        for i in report_readlines_json['results']:
            print(i['url'])
            


    
